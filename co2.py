import serial
import time
import subprocess
import traceback
import getrpimodel
import struct
import platform
import argparse
import sys
import json
import os.path
import tkinter as tk
import random
from tkinter import *
import logging

co2_measurement = 0

# setting
version = "0.3.9"
pimodel = getrpimodel.model
pimodel_strict = getrpimodel.model_strict()

if os.path.exists('/dev/serial0'):
    partial_serial_dev = 'serial0'
    print("device already configured as serial0")
elif pimodel == "3 Model B" or pimodel_strict == "3 Model B":
    partial_serial_dev = 'ttyS0'
    print("ttyS0 has been initiated")
elif pimodel_strict == "Zero W":
    partial_serial_dev = 'ttyS0'
    print("ttyS0 has been initiated")
else:
    partial_serial_dev = 'ttyAMA0'
    print("ttyAMA0 has been initiated")

serial_dev = '/dev/%s' % partial_serial_dev
stop_getty = 'sudo systemctl stop serial-getty@%s.service' % partial_serial_dev
start_getty = 'sudo systemctl start serial-getty@%s.service' % partial_serial_dev

# major version of running python
p_ver = platform.python_version_tuple()[0]


def set_serialdevice(serialdevicename):
    global serial_dev
    serial_dev = serialdevicename
    print("serial_dev:", serial_dev)


def connect_serial():
    return serial.Serial(serial_dev,
                         baudrate=9600,
                         bytesize=serial.EIGHTBITS,
                         parity=serial.PARITY_NONE,
                         stopbits=serial.STOPBITS_ONE,
                         timeout=1.0)


def mh_z19():
    try:
        ser = connect_serial()
        while 1:
            result = ser.write(b"\xff\x01\x86\x00\x00\x00\x00\x00\x79")
            s = ser.read(9)

            if p_ver == '2':
                if len(s) >= 4 and s[0] == "\xff" and s[1] == "\x86":
                    return {'co2': ord(s[2]) * 256 + ord(s[3])}
                break
            else:
                if len(s) >= 4 and s[0] == 0xff and s[1] == 0x86:
                    return {'co2': s[2] * 256 + s[3]}
                break
    except:
        traceback.print_exc()


def read():
    p = subprocess.call(stop_getty, stdout=subprocess.PIPE, shell=True)
    result = mh_z19()
    p = subprocess.call(start_getty, stdout=subprocess.PIPE, shell=True)
    if result is not None:
        return result


def read_all():
    p = subprocess.call(stop_getty, stdout=subprocess.PIPE, shell=True)
    try:
        ser = connect_serial()
        while 1:
            result = ser.write(b"\xff\x01\x86\x00\x00\x00\x00\x00\x79")
            s = ser.read(9)

            if p_ver == '2':
                if len(s) >= 9 and s[0] == "\xff" and s[1] == "\x86":
                    return {'co2': ord(s[2]) * 256 + ord(s[3]),
                            'temperature': ord(s[4]) - 40,
                            'TT': ord(s[4]),
                            'SS': ord(s[5]),
                            'UhUl': ord(s[6]) * 256 + ord(s[7])
                            }
                break
            else:
                if len(s) >= 9 and s[0] == 0xff and s[1] == 0x86:
                    return {'co2': s[2] * 256 + s[3],
                            'temperature': s[4] - 40,
                            'TT': s[4],
                            'SS': s[5],
                            'UhUl': s[6] * 256 + s[7]
                            }
                break
    except:
        traceback.print_exc()

    p = subprocess.call(start_getty, stdout=subprocess.PIPE, shell=True)
    if result is not None:
        return result


class FullScreenApp(object):
    padding = 3
    dimensions = "{0}x{1}+0+0"

    def __init__(self, master, **kwargs):
        self.master = master
        width = master.winfo_screenwidth() - self.padding
        height = master.winfo_screenheight() - self.padding
        master.geometry(self.dimensions.format(width, height))

        button_text = tk.StringVar()
        value = read()
        button_text.set(value)
        b = tk.Button(self.master, text="Exit", font=("Purisa", 20), command=lambda: self.pressed())
        button_text.set(json.dumps(value))
        b.place(relx=0.15, rely=0.9, anchor=tk.CENTER)

        off = tk.Button(self.master, text="Poweroff", font=("Purisa", 20), command=lambda: self.off_pressed())
        off.place(relx=0.8, rely=0.9, anchor=tk.CENTER)

        self.canvas = Canvas(self.master, width=400, height=100)
        bg = self.canvas.create_rectangle(0, 0, 400, 300, fill='green', tags=('bg_tag'))
        self.canvas.create_text(200, 60, anchor=tk.CENTER, text=value, font=("Purisa", 55), fill="white",
                                tags=('co2_tag'))
        self.canvas.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

    def pressed(self):
        print("clicked, exiting to desktop")
        sys.exit(0)

    def off_pressed(self):
        print("clicked, poweroff")
        os.system('systemctl poweroff')

    def ui_exception_message(exception):
        txt = exception
        app.canvas.itemconfigure('co2_tag', text=txt )
        app.canvas.after(1000, app.update_txt)

    def update_txt_and_color(self):
        try:
            value = read()
            if value:
                co2_measurement = value
                txt = 'CO2: '
                app.canvas.itemconfigure('co2_tag', text=txt + str(value["co2"]))
                app.canvas.after(3000, app.update_txt_and_color)
                reading = value["co2"]
                if reading < 800:
                    app.canvas.itemconfigure('bg_tag', fill='green')
                    app.canvas.after(3000, app.update_txt_and_color)
                elif reading < 1250:
                    app.canvas.itemconfigure('bg_tag', fill='yellow2')
                    app.canvas.after(3000, app.update_txt_and_color)
                elif reading >= 1250:
                    app.canvas.itemconfigure('bg_tag', fill='red')
                    app.canvas.after(3000, app.update_txt_and_color)
        except:
            print("no CO2 value received")
            exception = sys.exc_info()
            logging.warning(exception)
            app.canvas.itemconfigure('co2_tag', text=exception)


    def update_color(self):
        value = read()
        if value:
            reading = value["co2"]
            if reading < 800:
                app.canvas.itemconfigure('bg_tag', fill='green')
                app.canvas.after(3000, app.update_color)
            elif reading < 1250:
                app.canvas.itemconfigure('bg_tag', fill='yellow2')
                app.canvas.after(3000, app.update_color)
            elif reading >= 1250:
                app.canvas.itemconfigure('bg_tag', fill='red')
                app.canvas.after(3000, app.update_color)


def checksum(array):
    return struct.pack('B', 0xff - (sum(array) % 0x100) + 1)


if __name__ == '__main__':

    try:
        root = tk.Tk()
        root.configure(background='black')
        root.wm_attributes('-fullscreen', 'true')
        app = FullScreenApp(root)
    except:
        print("No display found")

    # while 1:

    #value = read()
    #print(json.dumps(value))
    # time.sleep(2)
    # FullScreenApp.draw()

    FullScreenApp.update_txt_and_color(root)
    # FullScreenApp.update_color(root) # don't need it since color is updated together with co2 reading
    #root.mainloop()
    tk.mainloop()



# ball = Ball(canvas, "red")
# ball.draw()  #Changed per Bryan Oakley's comment
# tk.mainloop()
